'use strict';
var _ = require('lodash'),
	moment = require('moment'),
	md5 = require('./tasks/utils/md5'),
	GPAssets = require('./tasks/utils/signature'),
	cssHash = require('./tasks/utils/cssHash'),
	cssBless = require('./tasks/utils/cssBless');


module.exports = function(grunt) {
	grunt.log.header = function() {};
	require('time-grunt')(grunt);

	var cssMap = grunt.file.readJSON('assets/sass/cssMap.json');

  	var scss = {
		options: {
			includePaths: ['libs/bootstrap-sass/assets/stylesheets'],
			outputStyle: 'expanded',
			line_comments: 'true'
		},
		files: _.transform(cssMap.compile, function(result, src, dest) {
	      result[dest.replace(/assets\/css\//gi, 'assets/sass/bless/')] = src;
	    })
	};

	
	grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    version: md5('<%= pkg.version %>'),
    assemble: {
      options: {
        layout: "web/layouts/default.html",
        flatten: true,
        data: 'web/data/*.{json,yml}',
        partials: ['web/includes/**/*.html' ],
        helpers: ['web/helpers/*.js']
      },
      pages: {
        files: {
          'dist/': ['web/pages/*.html']
        }
      }
	},
    sass: {
		dev: scss
	},
	bless: {
      dev: {
	      options: {
	        cacheBuster: true,
	        compress: false,
	        logCount : true
	      },
	      files:  _.transform(_.extend(cssMap.compile), function(result, src, dest) {
	      	var minHash = md5(grunt.file.readJSON('package.json').version);
					var newDest = dest.replace(/\.css$/gi, '.' + minHash + '.css');

          result[dest] = dest.replace(/assets\/css\//gi, 'assets/sass/bless/');
        })
	    },
	    deployment: {
	      options: {
	        cacheBuster: false,
	        compress: true,
	        logCount : true
	      },
	      files: cssBless(grunt,cssMap.optimize)
	    }
    },
    csscount: {
		  dev: {
		    src:  _.filter(_.keys(cssMap.compile), function(obj) {
				  return ~obj.toLowerCase().indexOf("common");
				}),
		    //src: cssHash(grunt,cssMap.optimize).files,
		    options: {
		      maxRules: 4000
		    }
		  }
	},
	cssmetrics: {
	    dev: {
	      src: _.filter(_.keys(cssMap.compile), function(obj) {
				  return  ~obj.toLowerCase().indexOf("common");
				}),
	      options: {
		      quiet: false,
		      maxSelectors: 4096,
		      maxFileSize: 10240000
		    }
	    }
    },
	sassDirection: _.toArray(cssMap.compile),
	cssmin: {
		dist: {
			files: 'dist/css/common.css'
		}
	},
    copy:{
      main: {
        files: [
          {
            expand: true,
            cwd : 'assets/' ,
            src: ['{images,fonts,css}/**/*'],
            dest: 'dist'
          }
        ],
      },
      fontAwesome:{
        files: [
          {
            expand: true,
            cwd : 'libs/font-awesome/fonts/' ,
            src: ['**/*'],
            dest: 'dist/fonts/'
          }
        ]
      },
      js:{
      	 files: [
          {
            expand: true,
            cwd : 'libs/jquery/dist/',
            src: ['**/*'],
            dest: 'dist/js/'
          },
          {
            expand: true,
            cwd : 'assets/js/',
            src: ['**/*'],
            dest: 'dist/js/'
          },
          {
            expand: true,
            cwd : 'libs/bootstrap-sass/assets/javascripts/',
            src: ['**/*'],
            dest: 'dist/js/'
          }
        ]
      }
    },
    clean: {
    	dist: ['dist/{**/*,!logs}'],
    	build:['assets/css','assets/sass/bless'],
    	dist: ['dist/css', 'dist/images', 'dist/js'],
	    html: ['dist/*.html'],
	    css: ['dist/css/*.css'],
        images: ['dist/images']
	},
    watch:{
      assemble:{
        files:[
          'web/includes/**/*.html', 
          'web/layouts/*.html', 
          'web/pages/**/*.html',
          'web/data/*.{json,yml}',
          'assets/js/*'
        ],
        tasks:['build']
      },
      sass:{
      	files:['assets/sass/**/**/*.{scss,sass}'],
      	tasks:['default']
      }
    }
  });
	
	grunt.loadTasks('tasks');

	require('load-grunt-tasks')(grunt);
  
  	grunt.loadNpmTasks('assemble');
  	grunt.loadNpmTasks('grunt-contrib-clean');
  	grunt.loadNpmTasks('grunt-contrib-uglify');
  	grunt.loadNpmTasks('grunt-contrib-watch');
  	grunt.loadNpmTasks('grunt-sass');
  	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-bless');
	grunt.loadNpmTasks('grunt-css-count');
	grunt.loadNpmTasks('grunt-css-metrics');

	grunt.task.registerTask('log', 'Create a new release build log files on each run.', function() {
		console.log('Date: '+ new Date());
		GPAssets();
	});

	grunt.registerTask('cssBuild','building GPAssets css', function () {
		grunt.log.writeln('Building GPAssets CSS Framework');
		grunt.log.writeln('---------------------------');
	});
	grunt.registerTask('hash','get hash for current version', function () {
		console.log('build Version : ', grunt.file.readJSON('package.json').version);
		console.log('build Hash : ', md5(grunt.file.readJSON('package.json').version));
	});
	grunt.registerTask('build', function() {
	  	grunt.config.set('pkgVersion', md5(grunt.file.readJSON('package.json').version));
		grunt.task.run(['buildCSS', 'copy', 'cssmin', 'assemble']);
	});
	
	grunt.registerTask('buildCSS', ['cssBuild', 'sass:dev', 'bless:dev']);

 	grunt.registerTask('default', ['clean:dist', 'log', 'hash', 'clean:dist', 'build', 'copy', 'clean:build']);
};