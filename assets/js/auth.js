$(document).ready(function(){
	let Alert = new alert($('.alert-msgs'));

	$.validator.addMethod("regx", function(value, element, regexpr) {          
	    return regexpr.test(value);
	});

	$('.loginForm').validate({
		rules:{
			email:{
				required: true,
				regx: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
			},
			password:{
				required: true,
				minlength: 8,
				regx: /[a-z].*[0-9]|[0-9].*[a-z]/i
			}

		},
		messages:{
			email:{
				required: 'Please fill out this field.',
				regx: 'Please enter a valid email.'
			},
			password:{
				required: 'Please fill out this field.',
				minlength: 'Password must be 8 characters minimum.',
				regx: 'Password must contains at least one digit and one letter.'
			}
		},
		submitHandler: function(form) {
		    if($('#email').val() === 'hello@ahmadali.me'){
		    	Alert.error('Bad email / password');
		    }else if($('#email').val() === 'aali@winfooz.com'){
		    	$('.other-auth-action').hide();
		    	$('#loginContent').hide();
		    	$('#loginMsgContent').fadeIn('slow'); 
		    }else{
		    	window.location = "home.html";
		    }
		}
	});

	$('.signupForm').validate({
		rules:{
			fName:{
				required: true
			},
			lName:{
				required: true
			},
			email:{
				required: true,
				regx: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
			},
			password:{
				required: true,
				minlength: 8,
				regx: /[a-z].*[0-9]|[0-9].*[a-z]/i
			},
			passwordConfirmation:{
				required: true,
				equalTo: "#password"
			}
		},
		messages:{
			fName:{
				required: 'Please fill out this field.'
			},
			lName:{
				required: 'Please fill out this field.'
			},
			email:{
				required: 'Please fill out this field.',
				regx: 'Please enter a valid email.'
			},
			password:{
				required: 'Please fill out this field.',
				minlength: 'Password must be 8 characters minimum.',
				regx: 'Password must contains at least one digit and one letter.'
			},
			passwordConfirmation:{
				required: 'Please fill out this field.',
				equalTo: 'Passwords dont match. Try again'
			}
		},
		submitHandler: function(form) {
		    if($('#email').val() === 'aali@winfooz.com'){
		    	Alert.error('Email is already exist.');
		    }else{
		    	$('.other-auth-action').hide();
		    	$('#signupContent').hide();
		    	$('#signupMsgContent').fadeIn('slow'); 
		    }
		}
	});

	$('.forgotPasswordForm').validate({
		rules:{
			email:{
				required: true,
				regx: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
			}
		},
		messages:{
			email:{
				required: 'Please fill out this field.',
				regx: 'Please enter a valid email.'
			}
		},
		submitHandler: function(form) {
		    if($('#email').val() === 'hello@ahmadali.me'){
		    	Alert.error('Bad email.');
		    }else{
		    	$('#forgotPasswordContent').hide();
		    	$('#forgotPasswordMsgContent').fadeIn('slow');
		    }
		}
	});

	$('.resetPasswordForm').validate({
		rules:{
			password:{
				required: true,
				minlength: 8,
				regx: /[a-z].*[0-9]|[0-9].*[a-z]/i
			},
			passwordConfirmation:{
				required: true,
				equalTo: "#password"
			}
		},
		messages:{
			password:{
				required: 'Please fill out this field.',
				minlength: 'Password must be 8 characters minimum.',
				regx: 'Password must contains at least one digit and one letter.'
			},
			passwordConfirmation:{
				required: 'Please fill out this field.',
				equalTo: 'Passwords dont match. Try again'
			}
		}
	});
	
});