$(document).ready(function(){
	$("#mileage").ionRangeSlider({
		min: 0,
	    max: 30000,
	    from: 10000,
	});

	$("#price").ionRangeSlider({
	    type: "double",	
	    min: 0,
	    max: 1000,
	    from: 200,
	    to: 800,
	    prefix: "$"
	});
	$("#year").ionRangeSlider({
	    type: "double",	
	    min: 1991,
	    max: 2017,
	    from: 2001,
	    to: 2010,
	    prefix: "Y"
	});
});