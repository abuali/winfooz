$(document).ready(function() {
    
    $('#bouncyflip').click(function(){
      var id = md5(makeid());
      sAlert.error('Your message Your message Your message Your message ', 
        { 
          id:id, 
          timeout: 50000, 
          position: 'top', 
          effect:'bouncyflip', 
          cta: {
            enable: true,
            html: null,
            class: 'default',
            href: '#',
            text: "hello"
          }
        }
      );
    });
    $('#flip').click(function(){
      var id = md5(makeid());
      sAlert.warning('Your message ', { id:id, timeout: 1000, position: 'bottom', effect:'flip'})
    });
    $('#genie').click(function(){
      var id = md5(makeid());
      sAlert.info('Your message ', 
        { 
          id:id, 
          timeout: 20000, 
          position: 'top-left', 
          effect:'genie',
          cta: {
            enable: true,
            html: null,
            class: 'btn btn-default',
            href: '#',
            text: "hello"
          }
        }
      )
    });
    $('#jelly').click(function(){
      var id = md5(makeid());
      sAlert.success('Your message', { id:id, timeout: 1000, position: 'top-right', effect:'jelly'})
    });
    $('#scale').click(function(){
      var id = md5(makeid());
      sAlert.error('Your message', { id:id, timeout: 1000, position: 'bottom-left', effect:'scale'})
    });
    $('#slide').click(function(){
      var id = md5(makeid());
      sAlert.warning('Your message', { id:id, timeout: 1000, position: 'bottom-right', effect:'slide'})
    });
    $('#stackslide').click(function(){
      var id = md5(makeid());
      sAlert.success('Your message', { id:id, timeout: 1000, position: 'bottom', effect:'stackslide'})
    });

    $('[data-toggle="tooltip"]').tooltip(); 
    $('.nav-toggle').click(function(){
  		$('.side-menu').toggleClass('active');
  		$('.nav-toggle').toggleClass('active');
  	});

    $(window).scroll(function (event) {
        if($(window).scrollTop() > 113){
            $('.navbar').removeClass('trans').addClass('fixed');
            $('.navbar .btn-default').removeClass('btn-white');
            $('.search').removeClass('full').addClass('scroll');
        }else{
            $('.navbar').removeClass('fixed').addClass('trans');
            $('.navbar .btn-default').addClass('btn-white');
            $('.search').removeClass('scroll').addClass('full');
        }
    });
    $('.dropdown-avatar-menu li.head').click(function(e){
      $(this).find('> i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
      $(this).find('ul.dealerships').toggleClass('active');
      $(this).parent().find('> li').not('.head,.divider').toggleClass('hide');
      e.stopPropagation();
    });
    $('.dealerships a').click(function(){
      $('.dealerships a i').removeClass('fa-check');
      $(this).find('i').addClass('fa-check');
      $('.dealershipname').text($(this).text());
    })

    sAlert.config({
        effect: 'flip',
        position: 'top-left',
        timeout: 5000,
        html: false,
        onRouteClose: true,
        stack: true,
        // or you can pass an object:
        // stack: {
        //     spacing: 10 // in px
        //     limit: 3 // when fourth alert appears all previous ones are cleared
        // }
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        beep: false,
        // examples:
        // beep: '/beep.mp3'  // or you can pass an object:
        // beep: {
        //     info: '/beep-info.mp3',
        //     error: '/beep-error.mp3',
        //     success: '/beep-success.mp3',
        //     warning: '/beep-warning.mp3'
        // }
        onClose: _.noop, //
        // examples:
        // onClose: function() {
        //     /* Code here will be executed once the alert closes. */
        // }
        cta: {
            enable: null,
            html: null,
            class: null,
            href: null,
            text: null
        }
    });
});

$(document).on('ready', function() {
  
  var slide = $('.slider-single');
  var slideTotal = slide.length - 1;
  var slideCurrent = -1;

  function slideInitial() {
    slide.addClass('proactivede');
    setTimeout(function() {
      slideRight();
    }, 500);
  }

  function slideRight() {
    if (slideCurrent < slideTotal) {
      slideCurrent++;
    } else {
      slideCurrent = 0;
    }

    if (slideCurrent > 0) {
      var preactiveSlide = slide.eq(slideCurrent - 1);
    } else {
      var preactiveSlide = slide.eq(slideTotal);
    }
    var activeSlide = slide.eq(slideCurrent);
    if (slideCurrent < slideTotal) {
      var proactiveSlide = slide.eq(slideCurrent + 1);
    } else {
      var proactiveSlide = slide.eq(0);

    }

    slide.each(function() {
      var thisSlide = $(this);
      if (thisSlide.hasClass('preactivede')) {
        thisSlide.removeClass('preactivede preactive active proactive').addClass('proactivede');
      }
      if (thisSlide.hasClass('preactive')) {
        thisSlide.removeClass('preactive active proactive proactivede').addClass('preactivede');
      }
    });
    preactiveSlide.removeClass('preactivede active proactive proactivede').addClass('preactive');
    activeSlide.removeClass('preactivede preactive proactive proactivede').addClass('active');
    proactiveSlide.removeClass('preactivede preactive active proactivede').addClass('proactive');
  }

  function slideLeft() {
    if (slideCurrent > 0) {
      slideCurrent--;
    } else {
      slideCurrent = slideTotal;
    }

    if (slideCurrent < slideTotal) {
      var proactiveSlide = slide.eq(slideCurrent + 1);
    } else {
      var proactiveSlide = slide.eq(0);
    }
    var activeSlide = slide.eq(slideCurrent);
    if (slideCurrent > 0) {
      var preactiveSlide = slide.eq(slideCurrent - 1);
    } else {
      var preactiveSlide = slide.eq(slideTotal);
    }
    slide.each(function() {
      var thisSlide = $(this);
      if (thisSlide.hasClass('proactivede')) {
        thisSlide.removeClass('preactive active proactive proactivede').addClass('preactivede');
      }
      if (thisSlide.hasClass('proactive')) {
        thisSlide.removeClass('preactivede preactive active proactive').addClass('proactivede');
      }
    });
    preactiveSlide.removeClass('preactivede active proactive proactivede').addClass('preactive');
    activeSlide.removeClass('preactivede preactive proactive proactivede').addClass('active');
    proactiveSlide.removeClass('preactivede preactive active proactivede').addClass('proactive');
  }
  var left = $('.slider-left');
  var right = $('.slider-right');
  left.on('click', function() {
    slideLeft();
  });
  right.on('click', function() {
    slideRight();
  });
  slideInitial();
});
   