module.exports = function () {
	function print() {

		console.log(' ');
		console.log('888       888 d8b           .d888                          ');
		console.log('888   o   888 Y8P          d88P"                           ');
		console.log('888  d8b  888              888                             ');
		console.log('888 d888b 888 888 88888b.  888888 .d88b.   .d88b. 88888888 ');
		console.log('888d88888b888 888 888 "88b 888   d88""88b d88""88b   d88P  ');
		console.log('88888P Y88888 888 888  888 888   888  888 888  888  d88P   ');
		console.log('8888P   Y8888 888 888  888 888   Y88..88P Y88..88P d88P    ');
		console.log('888P     Y888 888 888  888 888    "Y88P"   "Y88P" 88888888 ');
		console.log(' ');

	}

	return print();
};


